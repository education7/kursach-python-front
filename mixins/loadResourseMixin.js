export default {
    methods: {
        loadResource() {
            this.$axios.get(this.resource).then(response => this.items = response.data.data)
        },
        deleteResource(id) {
            this.$axios.delete(`${this.resource}/${id}`).then(response => this.loadResource())
        }
    }
}